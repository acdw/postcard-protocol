# Postcard protocol
## a simple protocol for short messages between friends

The postcard protocol attempts to emulate the experience of sending a short postcard to a friend over the open Internet.  Each message is a 512-byte UDP packet yeeted over the wire to the recipient's *post office*, which will hold the post card until such time as the recipient can pick it up and read it.

**NOTE** that the postcard protocol is a work in progress and will probably change like, a lot before 1.0.  So uh, don't expect much :)

## Postcards

A *postcard* is a single packet sent over the UDP protocol (the lossiness of UDP simulates the possibility of a postcard getting lost or damaged in the mail).  A packet is *safely* (for some values of "safe") no more than 512 bytes, so you can fit a short message (a couple of tweets' worth) in one.

Of course, that length is slightly shorter with the addressing and security requirements of a postcard.  A postcard transmission looks like this (fields are formatted as `<FIELD>`; they're explained further down):

```
<HEADER><RECIPIENT><MESSAGE><SENDER><SIGNATURE>
```

### Header

A postcard header carries the traditional "magic number" proclaiming its postcard-ness, as well as some metadata about the postcard itself.  Its format is as follows:

```
pc<VERSION><ENCODING><LENGTH>
```

- `pc` is the aforementioned "magic number" (28771, apparently).
- `VERSION` is the version number of the postcard protocol this postcard is in.  It's stored as a binary VERSION.REVISION pair, with each taking up 4 bits.  E.g., we're currently on version 0 (pre-release) revision 1, so 0.1 or as a byte, 00000001.  Version 3.4 would be 00110100.
- `ENCODING` is a one-byte indicator of which encoding this postcard uses.  See **Encodings**, below.
- `LENGTH` is the length of the message.  Since the entire postcard is at most 512 bytes, this field need only be 2 bytes long.

Altogether, the postcard header is 6 bytes.

### Recipient

The recipient field serves to address a postcard to a user and as a further anti-spam measure.  If the recipient of a message does not exist on a post office server, that server MUST discard the message and MAY tell the sender that it did so.

The recipient field is a single-byte address of a post office box belonging to a user.  Not only does this save space, it can also cut down on spam:

- It's recommended to only allow 128 users in a single post office.  By halving the attack area and randomly assigning addresses, a post office can guard against flooding by blocking senders who send to more than a set number of badly-addressed postcards.
- Even if the above scheme isn't used, a one-byte field limits a post office to only having 256 mailboxes.  If a server wants to run more than one post office, that's technically possible, but there's a pressure here to keep things small and friendly.

### Message

The postcard message is a free-form string of up to 409 bytes, in the encoding specified in the `ENCODING` indicator.

### Sender

To prevent spam, senders need a public-cryptography key pair to sign messages.  The shortest keys that I know of are Ed25519 keys, so that's what postcards should use to maximize the space for a message.

The public key field is 32 bytes.

### Signature

In order to ensure the sender is who they say they are, the entire postcard before this field MUST be signed cryptographically with the sender's private key.  This signature can then be used to check the veracity of the preceding information.

A ed25519 signature is 64 bytes.

## Encodings

In order to allow the greatest level of expression while keeping the messages as short as possible, the postcard protocol allows the use of old-school text encodings, or codepages, as well as accepting utf-8 strings.  The message part of the postcard is determined using the following schedule of encodings:

| Number | Encoding                                                          |
|--------|-------------------------------------------------------------------|
| 0      | UTF-8                                                             |
| 1      | ASCII                                                             |
| 2      | EBCDIC                                                            |
| 3      | ISO 8859-1 Western Europe                                         |
| 4      | ISO 8859-2 Western and Central Europe                             |
| 5      | ISO 8859-3 Western Europe and South European                      |
| 6      | ISO 8859-4 Western Europe and Baltic countries                    |
| 7      | ISO 8859-5 Cyrillic alphabet                                      |
| 8      | ISO 8859-6 Arabic                                                 |
| 9      | ISO 8859-7 Greek                                                  |
| 10     | ISO 8859-8 Hebrew                                                 |
| 11     | ISO 8859-9 Western Europe with amended Turkish character set      |
| 12     | ISO 8859-10 Western Europe with rationalized Nordic character set |
| 13     | ISO 8859-11 Thai                                                  |
| 14     | ISO 8859-13 Baltic languages plus Polish                          |
| 15     | ISO 8859-14 Celtic languages                                      |
| 16     | ISO 8859-15 ISO 8859-1 with rationalizations                      |
| 17     | ISO 8859-16 Central, Eastern and Southern European languages      |
| 18     | CP437                                                             |
| 19     | CP720                                                             |
| 20     | CP737                                                             |
| 21     | CP850                                                             |
| 22     | CP852                                                             |
| 23     | CP855                                                             |
| 24     | CP857                                                             |
| 25     | CP858                                                             |
| 26     | CP860                                                             |
| 27     | CP861                                                             |
| 28     | CP862                                                             |
| 29     | CP863                                                             |
| 30     | CP865                                                             |
| 31     | CP866                                                             |
| 32     | CP869                                                             |
| 33     | CP872                                                             |
| 34     | Windows-1250 for Central European languages that use Latin script |
| 35     | Windows-1251 for Cyrillic alphabets                               |
| 36     | Windows-1252 for Western languages                                |
| 37     | Windows-1253 for Greek                                            |
| 38     | Windows-1254 for Turkish                                          |
| 39     | Windows-1255 for Hebrew                                           |
| 40     | Windows-1256 for Arabic                                           |
| 41     | Windows-1257 for Baltic languages                                 |
| 42     | Windows-1258 for Vietnamese                                       |
| 43     | Mac OS Roman                                                      |
| 44     | KOI8-R                                                            |
| 45     | KOI8-U                                                            |
| 46     | KOI7                                                              |
| 47     | MIK                                                               |
| 48     | ISCII                                                             |
| 49     | TSCII                                                             |
| 50     | VISCII                                                            |
| 51     | Shift JIS                                                         |
| 52     | EUC-JP                                                            |
| 53     | ISO-2022-JP                                                       |
| 54     | JIS X 0213                                                        |
| 55     | Shift_JIS-2004                                                    |
| 56     | EUC-JIS-2004                                                      |
| 57     | ISO-2022-JP-2004                                                  |
| 58     | GB 2312                                                           |
| 59     | GBK (Microsoft Code page 936)                                     |
| 60     | GB 18030                                                          |
| 61     | Taiwan Big5 (a more famous variant is Microsoft Code page 950)    |
| 62     | Hong Kong HKSCS                                                   |
| 63     | Korean                                                            |
| 64     | EUC-KR                                                            |
| 65     | ISO-2022-KR                                                       |

These are pulled from Wikipedia's entry on [common character encodings][].  Revisions may be (probably are!) necessary.

[common character encodings]: https://en.wikipedia.org/wiki/Character_encoding#Common_character_encodings

## Post offices
TODO

## Sending a postcard
TODO

## Receiving postcards
TODO
